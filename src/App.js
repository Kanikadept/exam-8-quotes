import './App.css';
import Header from "./components/UI/Header/Header";
import Home from "./containers/Home/Home";
import {Switch, Route} from "react-router-dom";
import QuoteForm from "./containers/QuoteForm/QuoteForm";


const App = () => (
    <div className="App">
        <div className="container">
            <Header/>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/find/:id" exact component={Home}/>
                <Route path="/star-wars" exact component={Home}/>
                <Route path="/add-quote" component={QuoteForm}/>
                <Route path="/edit-quote/:id" render={props => (
                    <QuoteForm {...props}/>
                )}/>
            </Switch>

        </div>
    </div>
);

export default App;
