import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <div className="header">
            <span>Quotes Central</span>
            <nav className="header-nav">
                <NavLink to={'/'}>Quotes</NavLink>
                <NavLink to={'/add-quote'}>Submit new quote</NavLink>
            </nav>
        </div>
    );
};

export default Header;