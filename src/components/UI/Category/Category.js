const CATEGORIES = [
    {title: 'Star Wars', id: 'star-wars'},
    {title: 'Motivational', id: 'motivational'},
    {title: 'Humour', id: 'humour'},
    {title: 'Saying', id: 'saying'},
    {title: 'Famous people', id: 'famous people'},
]

export default CATEGORIES;