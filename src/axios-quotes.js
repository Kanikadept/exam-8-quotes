import axios from "axios";

const axiosQuotes = axios.create({
    baseURL: 'https://exam-8-quotes-kani-default-rtdb.firebaseio.com/'
})

export default axiosQuotes;