import React, {useState, useEffect} from 'react';
import './QuoteForm.css';
import CATEGORIES from "../../components/UI/Category/Category";
import axiosQuotes from "../../axios-quotes";

const QuoteForm = props => {

    const [quote, setQuote] = useState({
        category: 'star wars',
        author: '',
        quoteText: ''
    });

    const [isEdit, setIsEdit] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            const quoteResponse = await axiosQuotes.get('/quotes/' + props.match.params.id + '.json');
            setQuote(quoteResponse.data);
            setIsEdit(true);
        }
        if (props.match.params.id !== undefined) {
            fetchData().catch(console.error);
        }

    }, [props.match.params.id])

    const handleChange = (event) => {
        const {name, value} = event.target;
        setQuote(post => ({
            ...post, [name] : value
        }));
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        if(isEdit === true) {
            await axiosQuotes.put('/quotes/' + props.match.params.id + '.json', quote);
            props.history.push('/');
        } else {
            try {
                await axiosQuotes.post('/quotes.json', quote);
                props.history.push('/')
            } finally {

            }
        }

        setQuote({
            category: '',
            author: '',
            quoteText: ''
        });
    }

    return (
        <form className="quote-form" onSubmit={handleSubmit}>
            <p>Submit new quote</p>
            {quote.category}
            <label>Category</label>
            <select value={quote.category} className="categories" name="category" id="categories" onChange={handleChange} required>
                {CATEGORIES.map(category => {
                    return <option key={category.id} value={category.id}>{category.title}</option>
                })}
            </select>

            <label>Author</label>
            <input value={quote.author} className="author" name="author" type="text" onChange={handleChange}/>

            <label>Quote text</label>
            <textarea value={quote.quoteText} className="quote-text" name="quoteText" onChange={handleChange}/>

            {isEdit === true ? <button>Edit</button> : <button>Save</button>}
        </form>
    );
};

export default QuoteForm;