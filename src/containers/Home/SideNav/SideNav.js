import React from 'react';
import './SideNav.css';
import {NavLink} from "react-router-dom";

const SideNav = () => {
    return (
        <div className="side-nav">
            <ul>
                <li><NavLink to={'/'}>All</NavLink></li>
                <li><NavLink to={'/find/star wars'}>Star Wars</NavLink></li>
                <li><NavLink to={'/find/famous people'}>Famous people</NavLink></li>
                <li><NavLink to={'/find/saying'}>Saying</NavLink></li>
                <li><NavLink to={'/find/humour'}>Humour</NavLink></li>
                <li><NavLink to={'/find/motivational'}>Motivational</NavLink></li>
            </ul>
        </div>
    );
};

export default SideNav;