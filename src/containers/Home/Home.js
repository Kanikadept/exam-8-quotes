import React from 'react';
import './Home.css';
import SideNav from "./SideNav/SideNav";
import Quotes from "./Quotes/Quotes";

const Home = props => {


    return (
        <div className="home">
            <SideNav/>
            <Quotes propsR={props}/>
        </div>
    );
};

export default Home;