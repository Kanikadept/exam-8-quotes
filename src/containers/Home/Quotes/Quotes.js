import React, {useEffect, useState} from 'react';
import './Quotes.css';
import Quote from "./Quote/Quote";
import axiosQuotes from "../../../axios-quotes";

const Quotes = props => {

    const [quotes, setQuotes] = useState({});

    useEffect(() => {

        const fetchData = async () => {

            const quotesResponse = await axiosQuotes.get('/quotes.json');
            setQuotes(quotesResponse.data);
        }
        fetchData();

    }, [])

    useEffect(() => {
        const fetchData = async () => {
            const quotesResponse = await axiosQuotes.get(`/quotes.json?orderBy="category"&equalTo="${props.propsR.match.params.id}"`);
            setQuotes(quotesResponse.data);
            console.log(props.propsR.match.params.id)
        }
        if(props.propsR.match.params.id !== undefined) {
            fetchData();
        }

    }, [props.propsR.match.params.id])

    const handleRemoveFromState = (id) => {
        const quotesCopy = {...quotes};
        delete quotesCopy[id];
        setQuotes(quotesCopy);
    }

    return (
        <div className="quotes">
            {Object.keys(quotes).map(key => {
                return <Quote key={key} id={key}
                              quoteInfo={quotes[key]}
                              handleRemoveFromState={handleRemoveFromState}
                              propsR={props.propsR}/>
            })}
        </div>
    );
};

export default Quotes;