import React from 'react';
import './Quote.css';
import axiosQuotes from "../../../../axios-quotes";

const Quote = props => {

    const handleRemove = () => {
        const deleteData = async () => {

            await axiosQuotes.delete('/quotes/' + props.id + '.json');
            props.propsR.history.push('/');
            props.handleRemoveFromState(props.id);
        }
        deleteData().catch(console.error);
    }


    const handleEdit = () => {
        props.propsR.history.push('/edit-quote/' + props.id);
    }

    return (
        <div className="quote">
            <div className="quote_content">
                <p>{props.quoteInfo.quoteText}</p>
                <span>—{props.quoteInfo.author}</span>
            </div>
            <div className="btns">
                <button onClick={handleEdit}>Edit</button>
                <button onClick={handleRemove}>Delete</button>
            </div>
        </div>
    );
};

export default Quote;